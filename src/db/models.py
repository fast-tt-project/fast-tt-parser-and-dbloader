from peewee import *
from .secret import db_connection as dbc

db = MySQLDatabase(
    dbc.db_name,
    user=dbc.db_user,
    password=dbc.db_password,
    host=dbc.db_host,
    port=dbc.db_port
)


class FBaseModel(Model):
    class Meta:
        database = db


class Tazaq_Fasttimetable_Weekdays(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    name = CharField(191)
    slug = CharField(191, unique=True)


class Tazaq_Fasttimetable_Discipline_Types(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    name = CharField(191)
    description = TextField()


class Tazaq_Fasttimetable_Disciplines(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    name = CharField(191)
    description = TextField()
    metadata = TextField()


class Tazaq_Fasttimetable_TGC_Items(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    option_type = SmallIntegerField()
    name = CharField(191)
    slug = CharField(191, unique=True)
    alphabet = CharField(191)
    description = TextField()
    metadata = TextField()
    is_visible = BooleanField(default=False)


class Tazaq_Fasttimetable_Temp_Lessons(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    weekday_id = ForeignKeyField(Tazaq_Fasttimetable_Weekdays)
    discipline_id = ForeignKeyField(Tazaq_Fasttimetable_Disciplines)
    discipline_type_id = ForeignKeyField(Tazaq_Fasttimetable_Discipline_Types)
    teacher_id = ForeignKeyField(Tazaq_Fasttimetable_TGC_Items)
    group_id = ForeignKeyField(Tazaq_Fasttimetable_TGC_Items)
    classroom_id = ForeignKeyField(Tazaq_Fasttimetable_TGC_Items)
    l_date = DateField()
    l_begin = TimeField()
    l_end = TimeField()
    l_num = SmallIntegerField()
    hash_t = CharField(32, unique=True)


class Tazaq_Fasttimetable_Lessons(FBaseModel):
    id = IntegerField(primary_key=True).auto_increment
    weekday_id = ForeignKeyField(Tazaq_Fasttimetable_Weekdays)
    discipline_id = ForeignKeyField(Tazaq_Fasttimetable_Disciplines)
    discipline_type_id = ForeignKeyField(Tazaq_Fasttimetable_Discipline_Types)
    teacher_id = ForeignKeyField(Tazaq_Fasttimetable_TGC_Items)
    group_id = ForeignKeyField(Tazaq_Fasttimetable_TGC_Items)
    classroom_id = ForeignKeyField(Tazaq_Fasttimetable_TGC_Items)
    l_date = DateField()
    l_begin = TimeField()
    l_end = TimeField()
    l_num = SmallIntegerField()
    hash = CharField(32, unique=True)
