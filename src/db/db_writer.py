from .models import db, \
    Tazaq_Fasttimetable_Disciplines as Disciplines, \
    Tazaq_Fasttimetable_Discipline_Types as DisciplineTypes, \
    Tazaq_Fasttimetable_Weekdays as Weekdays, \
    Tazaq_Fasttimetable_Lessons as Lessons, \
    Tazaq_Fasttimetable_Temp_Lessons as TempLessons, \
    Tazaq_Fasttimetable_TGC_Items as TGCItems

from datetime import datetime
from slugify import slugify
from hashlib import md5

OT_TEACHERS = 1
OT_GROUPS = 2
OT_CLASSROOMS = 3


class DBWriter:
    def __init__(self):
        db.connect()
        self.data = {}

    def load(self, parsed_data: dict):
        self.data = parsed_data

    def save_teachers_groups(self):
        """ Сохранение преподов/групп """
        print("save TG")
        TGCItems.update(is_visible=False).execute()

        data = self.data.items()
        new_options = [
            {
                TGCItems.name: val['name'],
                TGCItems.slug: slugify(key),
                TGCItems.option_type: int(len(val['name']) < 15) + 1,
                TGCItems.is_visible: 1,
                TGCItems.alphabet: '1' + key.rjust(15, '0')[:15]
            }
            for key, val in data
        ]
        TGCItems.insert_many(new_options).on_conflict(update={TGCItems.is_visible: True}).execute()

    def save_lessons(self, move_to_stable=False, move_to_db_after_key=False):
        """ Сохранение занятий """
        print("save lessons")
        t, g, c, d, dt, w = self.__get_all_lists()
        new_lessons = []

        for key, val in self.data.items():
            # список занятий
            print(f"save lessons: {key}")
            lessons = val['data']

            for lesson in lessons:
                # дата + день недели
                l_date, weekday = self.__date_weekday(lesson[0])

                # запись+получение: день недели
                if not w.get(weekday):
                    w.update(self.__create_and_get(model=Weekdays, data={'name': weekday, 'slug': slugify(weekday)}))

                # запись+получение: группа
                g_slug4 = slugify(lesson[4])
                if not g.get(g_slug4):
                    g.update(self.__create_and_get(
                        model=TGCItems,
                        data={'slug': g_slug4, 'name': lesson[4], 'option_type': OT_GROUPS, 'alphabet': '1' + lesson[4].rjust(15, '0')[:15]},
                        sluggable=True))

                # запись+получение: дисциплина
                if not d.get(lesson[5]):
                    d.update(self.__create_and_get(model=Disciplines, data={'name': lesson[5]}))

                # запись+получение: тип дисциплины
                if not dt.get(lesson[6]):
                    dt.update(self.__create_and_get(model=DisciplineTypes, data={'name': lesson[6]}))

                # запись+получение: аудитория
                c_slug7 = slugify(lesson[7])
                if not c.get(c_slug7):
                    c.update(self.__create_and_get(
                        model=TGCItems,
                        data={'slug': c_slug7, 'name': lesson[7], 'option_type': OT_CLASSROOMS, 'alphabet': '1' + lesson[7].rjust(15, '0')[:15]},
                        sluggable=True))

                # запись+получение: аудитория
                t_slug8 = slugify(lesson[8])
                if not t.get(t_slug8):
                    t.update(self.__create_and_get(
                        model=TGCItems,
                        data={'slug': t_slug8, 'name': lesson[8], 'option_type': OT_TEACHERS, 'alphabet': '1' + lesson[8].rjust(15, '0')[:15]},
                        sluggable=True))

                lesson_str = "".join(list(map(lambda x: slugify(str(x)), lesson)))
                # формирование "строки"
                row = {
                    'l_date': l_date,
                    'weekday_id': w.get(weekday),
                    'l_begin': lesson[1].replace('.', ':'),
                    'l_end': lesson[2].replace('.', ':'),
                    'l_num': lesson[3],
                    'group_id': g.get(g_slug4),
                    'discipline_id': d.get(lesson[5]),
                    'discipline_type_id': dt.get(lesson[6]),
                    'classroom_id': c.get(c_slug7),
                    'teacher_id': t.get(t_slug8),
                    'hash_t': md5(bytes(lesson_str, 'utf-8')).hexdigest(),
                }

                # добавление в массив
                new_lessons.append(row)

            if move_to_db_after_key:
                print(f"insert lessons: {key}")
                Lessons.insert_many(new_lessons).on_conflict(update={'l_num': Lessons.l_num}).execute()
                new_lessons = []

        # опция, отвечающая за перенос данных в стабильную таблицу
        if move_to_stable:
            print("move lessons")
            tl = TempLessons
            l = Lessons
            l.insert_from(
                query=tl.select(tl.weekday_id, tl.discipline_id, tl.discipline_type_id, tl.teacher_id, tl.group_id,
                                tl.classroom_id, tl.l_date, tl.l_begin, tl.l_end, tl.l_num, tl.hash_t),
                fields=[l.weekday_id, l.discipline_id, l.discipline_type_id, l.teacher_id, l.group_id, l.classroom_id,
                        l.l_date, l.l_begin, l.l_end, l.l_num, l.hash]
            ).on_conflict_replace().execute()
            print("lessons moved")

        print("insert lessons")
        TempLessons.truncate_table()
        TempLessons.insert_many(new_lessons).on_conflict(update={'l_num': TempLessons.l_num}).execute()
        print("done")

    def __get_all_lists(self):
        """ Все списки, для сокращения количества обращений к БД """
        # преподы, группы, аудитории
        all_teachers = {}
        all_groups = {}
        all_classrooms = {}
        all_tgc = TGCItems.select(TGCItems.slug, TGCItems.id, TGCItems.option_type).dicts()
        for item in all_tgc:
            if item['option_type'] == 1:
                all_teachers[slugify(item['slug'])] = item['id']
            elif item['option_type'] == 2:
                all_groups[slugify(item['slug'])] = item['id']
            elif item['option_type'] == 3:
                all_classrooms[slugify(item['slug'])] = item['id']

        # предметы
        all_subjects = Disciplines.select(Disciplines.name, Disciplines.id).dicts()
        all_subjects = {item['name']: item['id'] for item in all_subjects}

        # тип лекции
        all_types = DisciplineTypes.select(DisciplineTypes.name, DisciplineTypes.id).dicts()
        all_types = {item['name']: item['id'] for item in all_types}

        # день недели
        all_weekdays = Weekdays.select(Weekdays.name, Weekdays.id).dicts()
        all_weekdays = {item['name']: item['id'] for item in all_weekdays}

        return all_teachers, all_groups, all_classrooms, all_subjects, all_types, all_weekdays

    def __date_weekday(self, string: str) -> (datetime, str):
        """Метод преобразования 15.06.20 (Понедельник) в 2020-06-15 и (Понедельник)
        :param string: строка от парсера
        :return: 2020-06-15 и (Понедельник)
        """
        splitted_date = string.split(' ')
        _date = splitted_date[0]
        _date = _date[:6] + '20' + _date[6:]
        _date = datetime.strptime(_date, '%d.%m.%Y').date()
        weekday = splitted_date[1]

        return _date, weekday

    @staticmethod
    def __create_and_get(model, data, sluggable=False):
        """ Запись в базу и возврат {slug: id} или {name: id} """
        empty_model = {'': 1}
        if sluggable:
            if not data.get('slug'):
                return empty_model
        else:
            if not data.get('name'):
                return empty_model
        model = model.create(**data)
        return {model.slug: model.id} if sluggable else {model.name: model.id}
