from datetime import datetime
import time

UPDATE_WEEKDAY = 2  # День недели обновления
UPDATE_HOURS = 15


def move_to_stable_time():
    t = time.localtime()
    hours_now = int(time.strftime("%H", t))
    return int(datetime.today().isoweekday()) == UPDATE_WEEKDAY and hours_now >= UPDATE_HOURS


def main():
    if path.isfile(data_json):
        data = json.loads(open(data_json).read())
    else:
        parser.get_all_timetable()
        data = parser.options
        with open(data_json, 'w') as f:
            json.dump(data, f)

    db_writer = DBWriter()
    db_writer.load(data)
    db_writer.save_teachers_groups()

    db_writer.save_lessons(move_to_stable=move_to_stable_time())

    remove(data_json)


if __name__ == "__main__":
    from src.parser.fast_tt_parser import FastTTParser
    from src.db.db_writer import DBWriter
    from os import remove, path
    import json

    data_json = 'data.json'
    parser = FastTTParser()

    main()
